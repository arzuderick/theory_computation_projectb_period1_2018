﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project_B
{
    class Program
    {
        static int non_t_index = 0;
        static List<String> results = new List<String>();
        static void Main(string[] args)
        {
            CFG cf_grammar = new CFG();
            cf_grammar.from_file("input_cfg.txt");
            String[] alphabet = cf_grammar.get_alphabet("input_alphabet.txt").ToArray();
            String[] words = ff_get_words("input_words.txt");
            int[] key_indexes = new int[cf_grammar.getDictionary().Keys.ToArray().Length];
            int input;
            bool bad_option = false;
            Console.WriteLine("-----   REMEMBER TO EDIT THESE FILES   -----\n\n- input_alphabet.txt\n- input_cfg.txt\n- input_words.txt\n");
            while (true)
            {
                Console.WriteLine("\n****************************************************\nThis is the alphabet provided");
                foreach (String symbol in alphabet)
                {
                    if (symbol.Equals("#"))
                        Console.WriteLine("ε");
                    else
                        Console.WriteLine(symbol);
                }
                Console.WriteLine("\nThis is the context-free grammar you entered");
                cf_grammar.printDictonary();
                Console.WriteLine("\nThese are the words you listed");
                foreach(String word in words)
                {
                    Console.WriteLine("- {0}", word);
                }
                Console.WriteLine("\n****************************************************");
                if (bad_option)
                {
                    Console.WriteLine("\n-----   PLEASE FOLLOW THE OPTIONS ACCORDINGLY   -----");
                }
                Console.WriteLine("\n-----   YOUR OPTIONS   -----\n\n1. Show Results, All looks good\n2. Show Edit Instructions, I don't think I understood well\n3. Exit, I need to make changes\n");
                Console.Write("CFG_Eval>");
                Int32.TryParse(Console.ReadLine(), out input);
                if (input == 1)
                {
                    Console.WriteLine("\n--------------------------------------------------------------------------------------");
                    evaluateWords(words, cf_grammar, key_indexes);
                    Console.WriteLine("--------------------------------------------------------------------------------------\n");
                    bad_option = false;
                }
                else if(input == 2)
                {
                    Console.WriteLine("\n------------------------------------------------------------------------------------------------------------------------------------------------------------------");
                    print_instructions();
                    Console.WriteLine("------------------------------------------------------------------------------------------------------------------------------------------------------------------\n");
                    bad_option = false;
                }
                else if (input == 3)
                {
                    Environment.Exit(0);
                }
                else
                {
                    bad_option = true;   
                }
            }
        }

        private static String [] ff_get_words(String file_name)
        {
            String full_path = "C:/Users/arzud/OneDrive/Documentos/Universidad/Sistemas/14-Teoria-Computacion/Project_B/Project_B/" + file_name;
            StreamReader input_reader = new StreamReader(full_path);
            String entry;
            List<String> words_list = new List<String>();
            while ((entry = input_reader.ReadLine()) != null)
            {
                words_list.Add(entry);
            }
            return words_list.ToArray();
        }

        private static Element[] expand(Element element, Dictionary<Element, Element[][]> productions, List<Element> expansion, int[] key_indexes, Random rand)
        {
            foreach (Element key in productions.Keys)
            {
                if (element.getDescription().Equals(key.getDescription()))
                {
                    break;
                }
                non_t_index++;
            }
           
            Element[][] production = productions[element];
            key_indexes[non_t_index] = rand.Next(0, production.Length);
            int pos = key_indexes[non_t_index];
            Element[] prod = production[pos];
            non_t_index = 0;
            for (int index = 0; index < prod.Length; index++)
            {
                if (prod[index].GetType() == typeof(NonTerminal))
                {
                    Element real = null;
                    foreach (Element key in productions.Keys)
                    {
                        if (prod[index].getDescription().Equals(key.getDescription()))
                        {
                            real = key;
                        }
                    }
                    expand(real, productions, expansion, key_indexes, rand);
                }
                else if (prod[index].GetType() == typeof(Terminal))
                {
                    expansion.Add(prod[index]);
                }
            }
            return expansion.ToArray();
        }

        private static List <String> getPossibilities(CFG cf_grammar, int[] key_indexes)
        {
            Random rnd = new Random();
            for (int i = 0; i < 1000; i++)
            {
                Element[] expansion = expand(cf_grammar.getStart(), cf_grammar.getDictionary(), new List<Element>(), key_indexes, rnd);
                List<String> result = new List<String>();
                foreach (Element e in expansion)
                {
                    result.Add(e.getDescription());
                }
                String pre_rm = String.Join("", result);
                String official = pre_rm.Replace("#", "");
                results.Add(official);
            }
            List<String> no_dup_results = results.Distinct().ToList();
            return no_dup_results;
        }

        private static void evaluateWords(String[] words, CFG cf_grammar, int[] key_indexes)
        {
            bool found = false;
            List<String> no_dup_results = getPossibilities(cf_grammar, key_indexes);
            foreach(String word in words)
            {
                foreach(String s in no_dup_results)
                {
                    if (word.Equals(s))
                    {
                        found = true;
                        Console.WriteLine("You made it < {0} >, congratulations!", word);
                        break;
                    }
                    else
                    {
                        found = false;
                    }
                }
                if(found == false)
                    Console.WriteLine("Thanks for your interest < {0} >, but no!", word);
            }
        }

        private static void print_instructions()
        {
            Console.WriteLine("\n-----   EDIT INSTRUCTIONS   -----\n\n*The input_cfg.txt file is for the context-free grammar, take into consideration that");
            Console.WriteLine("- The non-terminal on the left side mus be separated by a space from its productions, \n  e.g\n  S <Production>");
            Console.WriteLine("- A production consists of terminals and non-terminals, eache is represented the following way:\n  e.g\n  Terminals\n  {<Terminal Element>}\n\n  Non-Terminals\n  [<Non-Terminal Element>]");
            Console.WriteLine("- If the non-terminal has more than one production, these are separated by a space too\n  e.g\n  S <ProductionA> <ProductionB>\n");
            Console.WriteLine("*The input_alphabet.txt and the input_words files are for the alphabet and the words, respectively; the elements in those files are separated by a new line (Enter)\n");
        }
    }
}