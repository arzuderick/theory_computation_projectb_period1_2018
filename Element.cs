﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project_B
{
    class Element
    {
        String description;
        public Element() { }
        public Element(String description)
        {
            this.description = description;
        }

        public String getDescription()
        {
            return description;
        }
    }
}