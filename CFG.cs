﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Project_B
{
    class CFG
    {
        Element start;
        Dictionary<Element, Element[][]> cfg_productions;
        List<String> alphabet;

        public CFG()
        {
            this.start = new Element();
            this.cfg_productions = new Dictionary<Element, Element[][]>();
            this.alphabet = new List<String>();
        }

        public void from_file(String file_name)
        {
            String full_path = "C:/Users/arzud/OneDrive/Documentos/Universidad/Sistemas/14-Teoria-Computacion/Project_B/Project_B/" + file_name;
            StreamReader input_reader = new StreamReader(full_path);
            String entry;
            int start_flag = 0;
            while ((entry = input_reader.ReadLine()) != null)
            {
                parseEntry(entry, start_flag);
                start_flag++;
            }
        }

        public List<String> get_alphabet(String file_name)
        {
            String full_path = "C:/Users/arzud/OneDrive/Documentos/Universidad/Sistemas/14-Teoria-Computacion/Project_B/Project_B/" + file_name;
            StreamReader input_reader = new StreamReader(full_path);
            String entry;
            while ((entry = input_reader.ReadLine()) != null)
            {
                alphabet.Add(entry);   
            }
            return alphabet;
        }

        public void printDictonary()
        {
            foreach (Element key in cfg_productions.Keys)
            {
                Console.Write("{0} -> ", key.getDescription());
                for(int index = 0; index < cfg_productions[key].Length; index++)
                {
                    Element[] production = (cfg_productions[key])[index];
                    foreach(Element element in production)
                    {
                        if (element.getDescription().Equals("#"))
                            Console.Write("ε");
                        else
                            Console.Write(element.getDescription());
                    }
                    if (cfg_productions[key].Length > 1 && index != cfg_productions[key].Length - 1)
                        Console.Write("\n   | ");
                }
                Console.WriteLine();
            }
        }

        public Element getStart()
        {
            return start;
        }

        public Dictionary<Element, Element[][]> getDictionary()
        {
            return cfg_productions;
        }

        private void parseEntry(String entry, int start_flag)
        {
            String[] sectioned_entry = entry.Split(' ');
            String left_side_NT = sectioned_entry[0];
            Element key = new Element(left_side_NT);
            if(start_flag == 0)
            {
                start = key;
            }
            Element[][] values = values = new Element[sectioned_entry.Length - 1][];
            IEnumerable<String> non_terminals;
            IEnumerable<String> terminals;
            int dv_index = 0;
            for (int index = 1; index < sectioned_entry.Length; index++)
            {
                non_terminals = GetSubStrings(sectioned_entry[index], "[", "]");
                terminals = GetSubStrings(sectioned_entry[index], "{", "}");
                String[] arr_non_terminals = non_terminals.ToArray();
                String[] arr_terminals = terminals.ToArray();
                String production = sectioned_entry[index];
                int size_production = arr_non_terminals.Length + arr_terminals.Length;
                Element[] arr_production = new Element[size_production];
                int ap_index = 0;
                int at_index = 0;
                int an_index = 0;
                for(int p_index = 0; p_index < production.Length; p_index++)
                {
                    if(production[p_index] == '{')
                    {
                        arr_production[ap_index] = new Terminal(arr_terminals[at_index]);
                        ap_index++;
                        at_index++;
                    }
                    else if(production[p_index] == '[')
                    {
                        arr_production[ap_index] = new NonTerminal(arr_non_terminals[an_index]);
                        ap_index++;
                        an_index++;
                    }
                }
                values[dv_index] = new Element[size_production];
                values[dv_index] = arr_production;
                dv_index++;
            }
            cfg_productions.Add(key, values);
        }

        private IEnumerable<String> GetSubStrings(String input, String i_start, String i_end)
        {
            Regex entry = new Regex(Regex.Escape(i_start) + "(.*?)" + Regex.Escape(i_end));
            MatchCollection matches = entry.Matches(input);
            foreach (Match match in matches)
            {
                yield return match.Groups[1].Value;
            }
        }
    }
}